import 'package:flutter/material.dart';

const APP_TITLE = 'Star Wars Movies';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: APP_TITLE,
      theme: new ThemeData(
        primaryColor: Colors.black,
        primaryColorDark: Colors.black,
        primaryColorLight: Colors.white,
        accentColor: Colors.yellow,
        primaryTextTheme: TextTheme(title: TextStyle(color: Colors.yellow)),
      ),
      home: new MoviesPage(
        title: APP_TITLE,
      ),
    );
  }
}

class MoviesPage extends StatefulWidget {
  final String title;

  MoviesPage({Key key, @required this.title}) : super(key: key);

  @override
  MoviesPageState createState() => new MoviesPageState();
}

class MoviesPageState extends State<MoviesPage> {
  MoviesPageState();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: Center(
          child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                "Hello Star Wars fans welcome to the dark side of the force",
                style: Theme.of(context).textTheme.display1,
              ))),
    );
  }
}
