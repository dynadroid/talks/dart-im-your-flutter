import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:star_wars_movies/models/models.dart';

/// A simple API client to work with themoviedb.org api version 3
/// Enter your API at [_API_KEY]
class ApiClient {
  static const String _API_KEY = '1d51cef38e888586924becd5f1cc9b1d';
  static const String _BASE_ENDPOINT = 'api.themoviedb.org';
  static const _BASE_QUERY_PARAM = {
    'api_key': _API_KEY,
    'language': 'en-US',
    'include_adult': 'false'
  };

  final JsonDecoder _decoder = const JsonDecoder();

  Future<MovieSearchResultCollection> _getMovieSearchResults(Uri uri) async {
    final response =
        await http.get(uri, headers: {"Accept": "application/json"});

    final Map<String, dynamic> collectionResponseMap =
        _decoder.convert(response.body);

    return MovieSearchResultCollection.fromJson(collectionResponseMap);
  }

  Future<MovieSearchResultCollection> getMovieSearchResults() async {
    final params = Map.of(_BASE_QUERY_PARAM);
    params.addAll({'query': 'Star Wars', 'page': '1'});

    final Uri uri = Uri.https(_BASE_ENDPOINT, '/3/search/movie', params);

    return _getMovieSearchResults(uri);
  }

  Future<MovieSearchResultCollection> getMovieSearchResultsByPage(int page) {
    return null;
  }

  Future<Movie> getMovie(int id) async {
    return null;
  }
}
